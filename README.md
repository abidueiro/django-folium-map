Map to locate points of interest. Made with Django and Folium. It has different layers: satellite, contour lines, topographic, etc

It has a geocoding search box that translates addresses into coordinates and points them out on the map

![](screenshots/frontend.png)
![](screenshots/backend.png)
