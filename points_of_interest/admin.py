# Register your models here.

from django.contrib import admin
from points_of_interest.models import Points

# admin images

from django.utils.html import mark_safe


@admin.register(Points)
class PointAdmin(admin.ModelAdmin):
    list_display = ("point_name", "description", 'photo_preview')
    list_filter = ("place", )
        
    readonly_fields = ('foto_preview', )

    def foto_preview(self, obj):
        return obj.foto_preview

    foto_preview.short_description = 'Image preview'
    foto_preview.allow_tags = True
    
    pass

