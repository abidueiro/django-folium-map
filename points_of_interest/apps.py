from django.apps import AppConfig


class CasesConfig(AppConfig):
    name = 'points_of_interest'
