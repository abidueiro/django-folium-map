# Create your models here.

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django import forms
from django.utils.html import format_html
from django.utils.html import mark_safe


class Points(models.Model):
    place =( 
        ("1", "Monument"), 
        ("2", "Spring"), 
        ("3", "Natural_area"), 
    ) 
    point_name = models.TextField(max_length=20)
    description = models.TextField(max_length=200)
    place = models.CharField(max_length=300, choices = place, blank=True)
    uso = models.ManyToManyField
    latitude = models.FloatField(default=0, blank=True)
    longitude = models.FloatField(default=0, blank=True)
    photo = models.ImageField(upload_to="../media/", blank=True, null=True)

    @property
    def photo_preview(self):
        if self.photo:
            return mark_safe('<img src="{0}" width="150" height="150" />'.format(self.photo.url))
        else:
            return ('Without Image')

    class Meta:
        verbose_name_plural = "Points"
    
    def __str__(self):
        return f"{self.point_name}"
    
   

