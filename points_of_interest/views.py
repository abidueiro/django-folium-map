# Create your views here.
from django.shortcuts import render
import folium

# images popups
# import base64
import os

# Añadir leyenda
#from folium.plugins import FloatImage, Search
from folium import plugins
from django.views.generic import TemplateView
from points_of_interest.models import Points

# geocoding search
from geopy.geocoders import Nominatim
from django.db.models import Q
from django.shortcuts import redirect

def help(request):
    return render(request, 'help.html')

def color_producer(place):
    if place == '1':
        return 'green'
    elif place == '2':
        return 'orange'
    else:
        return 'red'


def show_map(request):  
    m = folium.Map([44.59, 0.53], zoom_start=7, name='Openstreetmaps')
    fg = folium.FeatureGroup(name = "Points")

    for obj in Points.objects.all():
        openstreetmap = "https://www.openstreetmap.org/search?query={}%2C%20{}".format( obj.latitude, obj.longitude )
        point_name = obj.point_name
        place = obj.place
        latitude = obj.latitude
        longitude = obj.longitude
        html = '''
                <h4>{point_name}</h4>
                <p>{latitude},{longitude}</p>
                <a href={openstreetmap} target="_blank">openstreetmaps</a><br>
                '''.format(point_name=point_name, latitude=latitude, longitude=longitude, 
                place=place, openstreetmap=openstreetmap)
        iframe = folium.IFrame(html, width=280, height=200)
        popup = folium.Popup(iframe, parse_html = True, max_width=200)
        fg.add_child(folium.Marker(location=[obj.latitude, obj.longitude], popup=popup,
        icon = folium.Icon(color = color_producer(obj.place))))
    
    # search box Nominatim with geolocation

    query = request.GET.getlist('q', '')
    if 'q' in request.GET != 'Nonetype':
        try:
            geolocator = Nominatim(user_agent="casas")
            location = geolocator.geocode(query[0])
            fg.add_child(folium.Marker(location=[location.latitude, location.longitude],
            icon = folium.Icon(color = ("blue"))))
        except:
            response = redirect('/help/')
            return response

    # map layers
        
    tile = folium.TileLayer('Stamen Terrain',name='Terrain').add_to(m)
    tile = folium.TileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
    attr="my data atribution", name='Satellite').add_to(m)
    tile = folium.TileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', attr="my data atribution",
    name='Topographical').add_to(m)
    tile = folium.TileLayer('https://{s}.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png', 
    attr="my data atribution", name='Paths').add_to(m)
    tile = folium.TileLayer('cartodbpositron',name='Simple').add_to(m)
    tile = folium.TileLayer('cartodbdark_matter', name='Simple-dark').add_to(m)
    
    # Show coordinates witha mouse click

    m.add_child(folium.LatLngPopup())

    
    m.add_child(fg)
    folium.LayerControl(collapsed=True).add_to(m)
    m = m._repr_html_() 
    
    context = {'my_map': m}
    return render(request, 'map.html', context)
    
 
 